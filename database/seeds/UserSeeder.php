<?php

use Illuminate\Database\Seeder;
use Faker\Factory as factory;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class, 10);
    }
}
